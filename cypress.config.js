const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    baseUrl: process.env.CI_ENVIRONMENT_URL || 'http://localhost:3000',
    video: false,
    setupNodeEvents(on, config) {
    },
  },
});
